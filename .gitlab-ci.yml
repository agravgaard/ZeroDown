---
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

variables:
  GL_CLUSTER_REG: registry.gitlab.com/gitlab-org/cluster-integration
  GL_HELM_REG: $GL_CLUSTER_REG/helm-install-image/releases
  GL_K3S_REG: $GL_CLUSTER_REG/test-utils/k3s-gitlab-ci/releases
  KUBE_IMG: $GL_HELM_REG/3.7.1-kube-1.20.11-alpine-3.14
  K3S_IMG: $GL_K3S_REG/v1.22.2-k3s2
  K3S_TOKEN: notsosecrettoken

shell-check:
  image: koalaman/shellcheck-alpine:stable
  stage: test
  before_script:
    - shellcheck --version
  script:
    - shellcheck ./scripts/*.sh
  rules:
    - changes:
        - scripts/*.sh
        - .gitlab-ci.yml
  needs: []

shell-format:
  image: mvdan/shfmt:v3.2.0-alpine
  stage: test
  before_script:
    - shfmt -version
  script:
    - shfmt -i 2 -ci -d ./**/*.sh
  rules:
    - changes:
        - "**/*.sh"
        - .gitlab-ci.yml
  needs: []

yaml-lint:
  image:
    name: cytopia/yamllint:latest
    entrypoint: ["/bin/sh", "-c"]
  script:
    - yamllint ./
  rules:
    - changes:
        - "**/*.yaml"
        - "**/*.yml"
        - .gitlab-ci.yml
  needs: []

build-docker:
  # Yanked with modification from: https://gitlab.com/ric_harvey/nginx-php-fpm
  image: docker:19.03-git
  services:
    - name: docker:19.03-dind
      command: ["--experimental"]
  before_script:
    - apk add make
    - echo Logging in to $CI_REGISTRY...
    - docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
    - export DOCKER_IMAGE="${CI_REGISTRY_IMAGE}/myimage"
  script:
    - make v123
    - docker image tag myimage:v1 ${DOCKER_IMAGE}:v1
    - docker image tag myimage:v2 ${DOCKER_IMAGE}:v2
    - docker image tag myimage:v3 ${DOCKER_IMAGE}:v3
    - docker image push ${DOCKER_IMAGE}
  rules:
    - changes:
        - Dockerfile
        - main.go
        - Makefile
        - .gitlab-ci.yml
  needs: []

k3s-test:
  image: $KUBE_IMG
  services:
    - name: $K3S_IMG
      alias: k3s
  before_script:
    - apk add curl
    - curl -f k3s:8081 > k3s.yaml
    - export KUBECONFIG=$(pwd)/k3s.yaml
    - kubectl version
    - kubectl cluster-info
  script:
    - kubectl apply -f k8s/namespaces.yaml
    - kubectl config set-context --namespace zerodown --current
    - kubectl apply -f k8s/
    - echo "Giving k3s some time to start up..."
    - sleep 90s
    - echo "1m 30s..."
    - kubectl get all --all-namespaces
    - sleep 10s
    - echo "1m 40s..."
    - kubectl get all
    - kubectl get ingress
    - ip=$(kubectl get ingress | grep -v NAME | awk '{ print $4 }')
    - hostname=$(kubectl get ingress | grep -v NAME | awk '{ print $3 }')
    - echo "$ip   $hostname" >> /etc/hosts
    - echo "$ip   $hostname"
    - curl -s $hostname/hello
    - echo "Try version 2"
    - sed -i.old "s|myimage:v1|myimage:v2|" k8s/zdapp.yaml
    - kubectl apply -f k8s/
    - ./scripts/timed_curl.sh 30
    - echo "We expect v1, because v2 is designed to never be ready:"
    - curl -s $hostname/hello | grep "1"
    - echo "Try version 3"
    - sed -i.old "s|myimage:v2|myimage:v3|" k8s/zdapp.yaml
    - kubectl apply -f k8s/
    - ./scripts/timed_curl.sh 30
    - echo "We expect v3:"
    - curl -s $hostname/hello | grep "3"
  needs:
    - job: build-docker
      optional: true
