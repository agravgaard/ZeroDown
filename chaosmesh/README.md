# [Chaos mesh](https://chaos-mesh.org/)

This directory contain the chaos-mesh installer and Kubernetes resiliency experiments.

## Install options
```bash
./install.sh --help
```
Check out [the docs](https://chaos-mesh.org/docs/quick-start/) for more info.

## Update install script

```bash
curl -sSL https://mirrors.chaos-mesh.org/v2.1.3/install.sh > install.sh
chmod +x install.sh
```

Format
```bash
shfmt -i 2 -ci install.sh > install-fmt.sh
mv install-fmt.sh install.sh
```

Apply lint
```bash
shellcheck -f diff ./install.sh > install.sh.patch
git apply -p 2 install.sh.patch
rm install.sh.patch
```

Uninstall is a call to `install.sh` and `kubectl`.
It won't need updating unless there's breaking changes in the install script.
