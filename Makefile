# build docker image for different versions

all: v1 v2 v3 v4 v5 v6 v7 v8 v9

v123: v1 v2 v3

v1: myimage-v1
v2: myimage-v2
v3: myimage-v3
v4: myimage-v4
v5: myimage-v5
v6: myimage-v6
v7: myimage-v7
v8: myimage-v8
v9: myimage-v9

myimage-v%: Dockerfile main.go
	$(eval vers := $(shell echo $@ | sed -nE 's/myimage-v([0-9]+)/\1/p'))
	echo $(vers)
	sed -i.old 's/version := 1/version := $(vers)/' main.go
	docker build -t myimage:v$(vers) .
	sed -i.old 's/version := $(vers)/version := 1/' main.go
	rm main.go.old
