#!/bin/sh
set -eu

end=$(($(date +%s) + $@))

while [ "$(date +%s)" -lt $end ]; do
  curl -s hello-world.info/hello
done
