package main

import (
	"fmt"
	"log"
	"net/http"
)

func handler(version int) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.URL.RawQuery)
		fmt.Fprintf(w, "Hello from version: %d\n", version)
	}
}

func healthz_handler(version int) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if version == 2 {
			w.WriteHeader(500)
			w.Write([]byte(fmt.Sprintf("error: Simulated error for %d", version)))
		} else {
			w.WriteHeader(200)
			w.Write([]byte("ok"))
		}
	}
}

func main() {
	version := 1
	http.HandleFunc("/hello", handler(version))

	http.HandleFunc("/healthz", healthz_handler(version))

	log.Fatal(http.ListenAndServe(":8080", nil))
}
