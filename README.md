# Zero downtime k8s upgrading

## Initial setup

### Create example k8s cluster

This example will use minikube
```
minikube start
minikube addons enable ingress
```

### Build the container
```
minikube docker-env

docker build -t myimage:v1 .
```

### Deploy application
First create and set the namespaces:
```
kubectl apply -f k8s/namespaces.yaml

kubectl config set-context --namespace zerodown --current
```

Then deploy the app:
```
kubectl apply -f k8s/
```

### Create the hostname

Get the address and hostname from the ingress (may take a minute or two):
```
kubectl get ingress
```

Append it at the end of `/etc/hosts`:
```
192.168.205.3   hello-world.info
```

### Test the connection:
```
curl hello-world.info
```

## Upgrade the app

### Build the v2 container
```
sed -i 's/version := 1/version := 2/' main.go

docker build -t myimage:v2 .
```

### Update the deployment
```
sed -i 's/myimage:v1/myimage:v2/' k8s/zdapp.yaml
```

### Open a two new terminals

 * `kubectl get pod -n zerodown -w`
 * `endless_curl.sh`

and in the original:
```
kubectl apply -f k8s/
```
