FROM golang:1.18-alpine AS build

WORKDIR /app/
COPY main.go main.go
RUN CGO_ENABLED=0 go build -o backend main.go

FROM scratch
COPY --from=build /app/backend /usr/local/bin/backend
CMD ["/usr/local/bin/backend"]
